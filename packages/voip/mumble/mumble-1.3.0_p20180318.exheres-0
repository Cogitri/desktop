# Copyright 2010-2011 Johannes Nixdorf <mixi@user-helfen-usern.de>
# Copyright 2012-2013 Lasse Brun <bruners@gmail.com>
# Copyright 2013-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'mumble-1.2.2.ebuild' from Gentoo, which is:
#     Copyright 1999-2010 Gentoo Foundation

CELT7_REV="5a16cda6d78cda0cd14eb13c56c65d82724842e5"
CELT11_REV="e3d39fec7c44d1841e817d3b1986bfdc4d0863a9"
MUMBLETHEME_REV="212f8e336d3c2b385c10a7462ceedb88919edd00"

require github [ user=${PN}-voip tag=b25db3e14fd17b168d9c90d5382ed0ef2c3730eb ] \
    qmake [ slot=5 ] \
    freedesktop-desktop \
    gtk-icon-cache \
    option-renames [ renames=[ 'speechd tts' ] ]

SUMMARY="Mumble is an open source, low-latency, high quality voice chat software"
DESCRIPTION="
Mumble is a voice chat application for groups. While it can be used for any kind of
activity, it is primarily intended for gaming. It can be compared to programs like Ventrilo or
TeamSpeak. People tend to simplify things, so when they talk about Mumble they either talk about
\"Mumble\" the client application or about \"Mumble & Murmur\" the whole voice chat application suite.
"
HOMEPAGE+=" https://wiki.${PN}.info/wiki/Main_Page"
DOWNLOADS+="
    https://github.com/mumble-voip/celt-0.7.0/archive/${CELT7_REV}.tar.gz -> celt-${CELT7_REV}.tar.gz
    https://github.com/mumble-voip/celt-0.11.0/archive/${CELT11_REV}.tar.gz -> celt-${CELT11_REV}.tar.gz
    https://github.com/mumble-voip/mumble-theme/archive/${MUMBLETHEME_REV}.tar.gz -> mumble-theme-${MUMBLETHEME_REV}.tar.gz
"

LICENCES="BSD-3 MIT"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    alsa
    avahi
    dbus
    oss
    pulseaudio
    tts [[ description = [ Support for text to speech ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        x11-libs/qttools:5 [[ note = [ Qt5LinguistTools ] ]]
        x11-proto/xorgproto
    build+run:
        dev-libs/boost
        dev-libs/protobuf:=
        media-libs/libsndfile
        media-libs/opus[>=1.0.0]
        media-libs/speex[>=1.2_rc1]
        media-libs/speexdsp[>=1.2_rc1]
        x11-libs/libX11
        x11-libs/libXi
        x11-libs/qtbase:5[gui][sql][sqlite]
        x11-libs/qtsvg:5
        alsa? ( sys-sound/alsa-lib )
        avahi? ( net-dns/avahi[dns_sd] )
        pulseaudio? ( media-sound/pulseaudio )
        tts? ( app-speech/speechd )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        !media-sound/mumble [[
            description = [ media-sound/mumble was moved to ::desktop voip/mumble ]
            resolution = uninstall-blocked-before
        ]]
"

DEFAULT_SRC_COMPILE_PARAMS=( -j1 )

src_prepare() {
    default

    # TODO: report upstream
    edo sed \
        -e "s:pkg-config:${PKG_CONFIG}:g" \
        -i main.pro \
        -i qmake/pkgconfig.pri \
        -i src/${PN}.pri \
        -i src/${PN}/${PN}.pro

    # set version
    edo sed \
        -e "s:Compiled by User:${PV}:g" \
        -i src/Version.h

    edo rmdir ${WORK}/3rdparty/celt-0.{7,11}.0-src
    edo ln -s ${WORKBASE}/celt-0.7.0-${CELT7_REV} ${WORK}/3rdparty/celt-0.7.0-src
    edo ln -s ${WORKBASE}/celt-0.11.0-${CELT11_REV} ${WORK}/3rdparty/celt-0.11.0-src

    edo rmdir ${WORK}/themes/Mumble
    edo ln -s ${WORKBASE}/mumble-theme-${MUMBLETHEME_REV} ${WORK}/themes/Mumble
}

src_configure() {
    config=(
        release                  # release build
        bundled-celt             # bundled celt
        no-bundled-opus          # bundled opus
        no-bundled-speex         # bundled speex
        no-embed-qt-translations # Qt translations embedding
        no-g15                   # Logitech g15 keyboard support
        no-server                # server part (voip/murmur)
        no-update                # new versions check
    )
    option alsa ||  config+=( no-alsa )
    option avahi || config+=( no-bonjour )
    option dbus || config+=( no-dbus )
    option oss || config+=( no-oss )
    option pulseaudio || config+=( no-pulseaudio )
    option tts || config+=( no-speechd )

    eqmake main.pro -recursive \
        CONFIG+="${config[*]}" \
        DEFINES+="PLUGIN_PATH=/usr/$(exhost --target)/lib/mumble"
}

src_install() {
    local host=$(exhost --target)

    dobin release/${PN}
    dobin scripts/${PN}-overlay

    exeinto /usr/${host}/lib/${PN}
    doexe release/libmumble.so.$(ever range 1-3)
    dosym libmumble.so.$(ever range 1-3) /usr/${host}/lib/${PN}/libmumble.so
    dosym libmumble.so.$(ever range 1-3) /usr/${host}/lib/${PN}/libmumble.so.$(ever range 1)
    dosym libmumble.so.$(ever range 1-3) /usr/${host}/lib/${PN}/libmumble.so.$(ever range 1-2)
    doexe release/plugins/lib*.so*
    doexe release/libcelt0.so.0.{7,11}.0

    insinto /usr/share/applications
    doins scripts/${PN}.desktop

    insinto /usr/share/icons/hicolor/scalable/apps
    doins icons/${PN}.svg

    insinto /usr/share/kde4/services
    doins scripts/${PN}.protocol

    doman man/${PN}.1
    doman man/${PN}-overlay.1

    emagicdocs
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst

    elog "Mumble supports reading the kernel input devices, but may fall back to using the less optimal xinput2"
    elog "This can be solved with a simple udev rule: SUBSYSTEM==\"input\", GROUP=\"input\" MODE=\"660\""
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

