# Copyright 2016-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require meson [ meson_minimum_version=0.42 ] test-dbus-daemon

export_exlib_phases src_test

SUMMARY="Tools and libraries to work with AppStream XML/YAML metadata"
DESCRIPTION="
AppStream is a cross-distribution effort for enhancing the way we interact with
the software repositories provided by (Linux) distributions by standardizing
software component metadata.

It provides the foundation to build software-center applications, by providing
metadata necessary for an application-centric view on package repositories.
AppStream additionally provides specifications for things like an unified
software metadata database, screenshot services and various other things needed
to create user-friendly application-centers for (Linux) distributions."

HOMEPAGE="https://www.freedesktop.org/wiki/Distributions/AppStream/"
DOWNLOADS="https://www.freedesktop.org/software/${PN}/releases/AppStream-${PV}.tar.xz"

UPSTREAM_DOCUMENTATION="https://www.freedesktop.org/software/${PN}/docs/"

LICENCES="
    BSD-3 [[ note = [ cmake scripts ] ]]
    GPL-2 LGPL-2.1
"
SLOT="0"
MYOPTIONS="doc gobject-introspection qt5"

DEPENDENCIES="
    build:
        app-text/docbook-xml-dtd:4.5
        dev-libs/libxslt [[ note = [ xsltproc ] ]]
        dev-util/itstool
        sys-devel/gettext
        virtual/pkg-config
        doc? ( dev-doc/gtk-doc )
    build+run:
        dev-libs/glib:2[>=2.46]
        dev-libs/libxml2:2.0
        dev-libs/libyaml
        gobject-introspection? ( gnome-desktop/gobject-introspection[>=1.54] )
        qt5? ( x11-libs/qtbase:5 )
    suggestion:
        net-misc/curl [[ description = [ URL validation with appstreamcli ] ]]
"

MESON_SOURCE="${WORKBASE}/AppStream-${PV}"

MESON_SRC_CONFIGURE_PARAMS+=(
    -Dapt-support=false
    # Needs python3 and https://fedorahosted.org/publican/, the latter unwritten
    -Ddocs=false
    # Needs http://snowball.tartarus.org/dist/libstemmer_c.tgz/
    # https://github.com/zvelo/libstemmer - unreleased and unwritten
    -Dstemming=false
    # Feel free to make this optional if you need it.
    -Dvapi=false
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES+=(
    'doc apidocs'
    'gobject-introspection gir'
    'qt5 qt'
)

appstream_src_test() {
    test-dbus-daemon_start
    meson_src_test
    test-dbus-daemon_stop
}
