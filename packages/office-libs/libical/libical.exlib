# Copyright 2008, 2009, 2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2013, 2016-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever at_least 3.0.0 ; then
    require github [ tag=v${PV} ] cmake [ api=2 cmake_minimum_version=3.0.0 ]
else
    require github [ tag=v${PV} ] cmake [ api=2 ]
fi
require alternatives

export_exlib_phases src_compile src_install

SUMMARY="An implementation of the IETF's iCalendar Calendaring and Scheduling protocols"
DESCRIPTION="
ibical is an Open Source implementation of the IETF's iCalendar Calendaring and Scheduling protocols.
(RFC 2445, 2446, and 2447).
It parses iCal components and provides C/C++/Python/Java APIs for manipulating the component properties,
parameters, and subcomponents.
"

LICENCES="|| ( MPL-1.1 LGPL-2.1 )"
SLOT="$(ever major)"
MYOPTIONS="doc gobject-introspection"

DEPENDENCIES="
    build:
        dev-lang/perl:*
        doc? ( app-doc/doxygen )
    build+run:
        dev-libs/icu:=   [[ note = [ higly recommended RSCALE support ] ]]
        gobject-introspection? (
            gnome-desktop/gobject-introspection:1[>=0.6.7]
        )
    run:
        sys-libs/timezone-data
        !office-libs/libical:0[<2.0.0-r2] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

if ever at_least 3.0.0 ; then
    DEPENDENCIES+="
        build:
            virtual/pkg-config
        build+run:
            dev-libs/glib[>=2.32]
            dev-libs/libxml2[>=2.7.3]
    "
    CMAKE_SRC_CONFIGURE_PARAMS+=(
        -DCMAKE_DISABLE_FIND_PACKAGE_BDB:BOOL=true
        -DICAL_GLIB_VAPI:BOOL=false
        -DICAL_GLIB:BOOL=true
    )
else
    CMAKE_SRC_CONFIGURE_PARAMS+=(
        # Berkeley DB storage - "MAY BE BUGGY. DO NOT USE FOR PRODUCTION"
        -DWITH_BDB:BOOL=false
    )
fi

# Missing files from archive
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DICAL_ERRORS_ARE_FATAL:BOOL=false
    -DSHARE_INSTALL_DIR=/usr/share
    -DSHARED_ONLY:BOOL=TRUE
    -DUSE_BUILTIN_TZDATA:BOOL=false
)
CMAKE_SRC_CONFIGURE_OPTIONS+=( 'gobject-introspection GOBJECT_INTROSPECTION' )
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS=( 'doc Doxygen' )

DEFAULT_SRC_INSTALL_EXTRA_DOCS=(
    doc/AddingOrModifyingComponents.txt
    doc/UsingLibical.txt
)

libical_src_compile() {
    default
    option doc && emake docs
}

libical_src_install() {
    local arch_dependent_alternatives=()
    local host=$(exhost --target)

    cmake_src_install

    arch_dependent_alternatives+=(
        /usr/${host}/include/${PN}          ${PN}-${SLOT}
        /usr/${host}/lib/cmake/LibIcal      LibIcal-${SLOT}
        /usr/${host}/lib/${PN}_cxx.so       ${PN}_cxx-${SLOT}.so
        /usr/${host}/lib/${PN}.so           ${PN}-${SLOT}.so
        /usr/${host}/lib/${PN}ss.so         ${PN}ss-${SLOT}.so
        /usr/${host}/lib/${PN}ss_cxx.so     ${PN}ss_cxx-${SLOT}.so
        /usr/${host}/lib/${PN}vcal.so       ${PN}vcal-${SLOT}.so
        /usr/${host}/lib/pkgconfig/${PN}.pc ${PN}-${SLOT}.pc
    )

    if option doc ; then
        dodoc -r apidocs/html
    fi

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
}

