# Copyright 2009, 2010, 2011 Mike Kelly
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'opera-9.63.exheres-0' from ::voroskoi, which is:
#     Copyright (c) 2008-2009 András Vöröskői <voroskoi@gmail.com>

export_exlib_phases src_install

myexparam rev
myexparam suffix
myexparam base_url="ftp://ftp.opera.com/pub/opera/linux/${PV//./}"
myexparam -b opera_next=false

MY_PV="${PV%%_*}-$(exparam rev)"
MY_PN=opera
exparam -b opera_next && MY_PN=opera-next
MY_PNV="${MY_PN}-${MY_PV}"

SUMMARY="The Opera web browser"
HOMEPAGE="http://www.opera.com/"

DOWNLOADS="
listed-only:
    platform:x86? ( $(exparam base_url)/${MY_PNV}.i386.linux$(exparam suffix) )
    platform:amd64? ( $(exparam base_url)/${MY_PNV}.x86_64.linux$(exparam suffix) )
"

LICENCES="opera"
SLOT="0"
MYOPTIONS="
platform:
    amd64 x86"

DEPENDENCIES="
    run:
        dev-db/sqlite:3
        dev-libs/atk
        dev-libs/glib:2
        dev-libs/libxml2:2.0
        media-libs/fontconfig
        media-libs/freetype:2
        media-libs/gstreamer:0.10
        net-misc/curl
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libXext
        x11-libs/libXft
        x11-libs/libXrender
        x11-libs/pango
        x11-libs/qt:4
    suggestion:
        kde/kdelibs:4 [[ description = [ Allow Opera to use use the KDE Qt widget style ] ]]
        media-plugins/gst-plugins-base:0.10 [[ description = [ Allow Opera to use the HTML5 video and audio tags ] ]]
        x11-libs/gtk+:* [[ description = [ Allow Opera to use the system GTK+ widget style ] ]]
"

UPSTREAM_CHANGELOG="http://www.opera.com/docs/changelogs/unix/${PV/./}/"

WORK="${WORKBASE}"

opera_src_install() {
    cd "${WORKBASE}"/${MY_PNV}*

    TMPDIR="${TEMP}" edo ./install \
        --unattended \
        --prefix /usr/ \
        --name opera \
        --repackage "${IMAGE}"/usr

    edo mv "${IMAGE}"/usr/share/doc/{${PN},${PNVR}}
    dodir /usr/$(exhost --target)
    edo mv "${IMAGE}"/usr{,/$(exhost --target)}/lib
    edo mv "${IMAGE}"/usr{,/$(exhost --target)}/bin

    insinto /etc/revdep-rebuild
    hereins ${PN} <<EOF
SEARCH_DIRS_MASK=/usr/$(exhost --target)/lib/opera/
EOF
}

