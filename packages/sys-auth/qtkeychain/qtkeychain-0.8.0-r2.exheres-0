# Copyright 2013-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=frankosterfeld tag=v${PV} ] cmake [ api=2 cmake_minimum_version=2.8.11 ]

SUMMARY="Platform-independent Qt API for storing passwords securely"
DESCRIPTION="
QtKeychain is a Qt API to store passwords and other secret data securely. If running, GNOME Keyring
is used, otherwise qtkeychain tries to use KWallet (via D-Bus), if available. In unsupported
environments QtKeychain will report an error. It will not store any data unencrypted unless
explicitly requested (setInsecureFallback( true )).
"

LICENCES="BSD-2"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~x86"
MYOPTIONS="
    libsecret [[ description = [ Libsecret storage backend ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        x11-libs/qttools:5 [[ note = [ for Qt5LinguistTools ] ]]
    build+run:
        x11-libs/qtbase:5
        libsecret? ( dev-libs/libsecret:1 )
    suggestion:
        gnome-desktop/gnome-keyring:1 [[ description = [ GNOME Keyring storage backend ] ]]
        kde/kwalletmanager:4 [[ description = [ KWallet storage backend (KF5) ] ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DQTKEYCHAIN_STATIC:BOOL=FALSE
    -DBUILD_TRANSLATIONS:BOOL=TRUE
    -DBUILD_WITH_QT4:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'libsecret LIBSECRET_SUPPORT'
)

