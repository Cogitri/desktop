# Copyright 2012 Johannes Nixdorf <mixi@exherbo.org>
# Copyright 2010 Jonathan Dahan <jedahan@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require pwmt

export_exlib_phases src_prepare src_compile src_install

SUMMARY="Highly customizable and functional PDF viewer based on poppler/GTK+"

LICENCES="ZLIB"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-util/intltool
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.32]
        sys-apps/file                       [[ description = [ Support mimetype detection with libmagic ] ]]
        x11-libs/cairo
        x11-libs/girara[>=0.2.6]
        x11-libs/gtk+:3[>=3.6]
    test:
        dev-libs/check
    recommendation:
        virtual/zathura-pdf                 [[ description = [ Used to view PDF documents ] ]]
    suggestion:
        text-plugins/zathura-djvu           [[ description = [ Used to view djvu documents ] ]]
        text-plugins/zathura-ps             [[ description = [ Used to view postscript documents ] ]]
        text-plugins/zathura-cb             [[ description = [ Used to view comic books ] ]]
"

# they require a running x server
RESTRICT="test"

BUGS_TO="jedahan@gmail.com"

DEFAULT_SRC_COMPILE_PARAMS=(
    COLOR=0
    PREFIX=/usr/$(exhost --target)
    WITH_SQLITE=0
    WITH_SYSTEM_SYNCTEX=0
)

DEFAULT_SRC_INSTALL_PARAMS=( "${DEFAULT_SRC_COMPILE_PARAMS[@]}" )

zathura_src_prepare() {
    default

    edo sed -e 's/pkg-config/\${PKG_CONFIG}/' \
            -e 's:\${PREFIX}/share:/usr/share:' \
            -i Makefile {,tests/}config.mk
}

zathura_src_compile() {
    unset LINGUAS # used in po/Makefile

    default
}

zathura_src_install() {
    unset LINGUAS

    default
}

